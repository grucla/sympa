#include <typeinfo>
#include "sympa.h"

#include "breadth_first_search.h"
#include "uct_pdbs.h"

#include "../task_proxy.h"
#include "../utils/system.h"
#include "../utils/timer.h"
#include "../utils/debug_macros.h"
#include "../option_parser.h"
#include "../plugin.h"
#include "../cudd-3.0.0/dddmp/dddmp.h"
#include "../cudd-3.0.0/cudd/cudd.h"

using namespace std;
using utils::g_timer;
using utils::Timer;

namespace symbolic {

SymPA::SymPA(const Options &opts) : 
    SearchEngine(opts), SymController(opts),
    searchDir(Dir(opts.get_enum("search_dir"))),
    abstractDir(Dir(opts.get_enum("abstract_dir"))),
    mgrParams(opts), searchParams(opts), 
    phTime(opts.get<double> ("ph_time")), phMemory(opts.get<double> ("ph_memory")), 
    maxRelaxTime(opts.get<int> ("max_relax_time")), 
    maxRelaxNodes(opts.get<int> ("max_relax_nodes")), 
    absTRsStrategy (AbsTRsStrategy(opts.get_enum("tr_st"))),
    insert_dead_ends (opts.get<bool>("insert_dead_ends")),
    perimeterPDBs (opts.get<bool>("perimeter_pdbs")), 
    ratioRelaxTime(opts.get<double> ("relax_ratio_time")), 
    ratioRelaxNodes(opts.get<double> ("relax_ratio_nodes")), 
    multiply_time_by (opts.get<double> ("multiply_time_by")), 
    num_fails_to_multiply_time(opts.get<int> ("num_fails_to_multiply_time")),
    time_fails_to_multiply_time(opts.get<int> ("time_fails_to_multiply_time")), 
    shouldAbstractRatio(opts.get<double> ("should_abstract_ratio")), 
    maxNumAbstractions(opts.get<int> ("max_abstractions")), 
    UCT_C(opts.get<double> ("uct_c")),
    rewardType (UCTRewardType(opts.get_enum("reward_type"))), 
    RAVE_K (opts.get<double> ("rave_k")),
    add_abstract_to_ongoing_searches_time (opts.get<int>( "add_abstract_to_ongoing_searches_time")),
    task_proxy(*g_root_task()),
    numAbstractions(0), num_iterations_without_reward (0),time_last_reward(g_timer()), 
    time_step_abstract(0), time_step_original(0), 
    time_select_exploration(0),  time_notify_mutex(0), 
    time_init(0) {  
}


void SymPA::initialize(){
    print_options();

    tree = make_unique<UCTTree> (vars.get(), mgrParams, OperatorCost::ONE);

    if (searchDir != Dir::BW) {
	ongoing_searches.push_back(tree->getRoot()->initSearch(true, searchParams));
    }

    if (searchDir != Dir::FW){
	ongoing_searches.push_back(tree->getRoot()->initSearch(false, searchParams));
    }
    time_init = g_timer();
}

void SymPA::insertDeadEnds(BDD bdd, bool isFW) {
    assert(tree);
    if(isFW){
	dead_end_fw.push_back(bdd);
	tree->getRoot()->getMgr()->mergeBucket(dead_end_fw);
    } else {
	dead_end_bw.push_back(bdd);
	tree->getRoot()->getMgr()->mergeBucket(dead_end_bw);
    }

    //Propagate to symbolic managers
    tree->getRoot()->propagateNewDeadEnds(bdd, isFW);   
}


UCTNode * SymPA::relax(UCTNode * node,
			    bool fw,  
			    std::vector<UCTNode *> & uct_trace, 
			    bool override_search) {
    DEBUG_MSG(cout << "SymPA::relax" << endl;);
    Timer t_relax;
    shared_ptr<BreadthFirstSearch> searchToRelax = node->retrieveSearch(fw, override_search);
    shared_ptr<SymStateSpaceManager> parentMgr = node->getMgr();
    
    while(node && node->isAbstractable()) {
	
	node->initChildren();
	if (rewardType == UCTRewardType::RAND) {
	    node = node->getRandomChild(fw);
	} else {
	    node = node->getChild(fw, UCT_C, RAVE_K, tree->getRoot());
	}
	
	if(!node) break; 

	uct_trace.push_back(node);

	if (node->getSearch(fw)) {
	    searchToRelax = node->retrieveSearch(fw, override_search);
	    continue;
	}

	if(!node->getMgr()) {	    
	    node->init(parentMgr, absTRsStrategy);
	} 

	parentMgr = node->getMgr();


	auto res = node->relax(searchToRelax, searchParams, 
			       maxRelaxTime, maxRelaxNodes,
			       ratioRelaxTime, ratioRelaxNodes, 
			       perimeterPDBs);

	if (res) {
	    node->getMgr()->addDeadEndStates(dead_end_fw, dead_end_bw);
	    time_select_exploration += t_relax();
	    return node;
	}
    }

    time_select_exploration += t_relax();
    return nullptr;
}
       
SearchStatus SymPA::step(){
    BreadthFirstSearch * currentSearch = selectExploration();

    Timer timer; 
    if(currentSearch){
	currentSearch->step();
    
	if(!currentSearch->isAbstracted()){  // current search is in original state space
	    time_step_original += timer();

	    if (currentSearch->finished()) {
		if (currentSearch->foundSolution()){
		    Plan empty_plan;
		    set_plan(empty_plan);
		    return SOLVED;
		} else {
            generateUnsolvabilityProof(currentSearch);
		    return FAILED;
		} 
	    }	
	}else{  // current search is in abstract state space
	    DEBUG_MSG(cout << "Have I finished the abstract search?" << endl;);
	    if (currentSearch->finished()) {
		DEBUG_MSG(cout << "Abstract search finished" << endl;);
	    
		notifyFinishedAbstractSearch(currentSearch);
		auto it = std::find(ongoing_searches.begin(), 
				    ongoing_searches.end(), 
				    currentSearch);

		ongoing_searches.erase(it);
	    }

	    time_step_abstract += timer();	
	}
    }
    return IN_PROGRESS;
}

void SymPA::increase_num_iterations_without_reward(){
    num_iterations_without_reward ++;
    if(num_iterations_without_reward > num_fails_to_multiply_time && 
       g_timer() - time_last_reward > time_fails_to_multiply_time) {
	searchParams.maxStepNodesMin *= multiply_time_by;
	cout << "Increased node bound: " << searchParams.getMaxStepNodes() << endl;
	num_iterations_without_reward = 0;
	time_last_reward = g_timer();
    }
}

void SymPA::notifyFinishedAbstractSearch(BreadthFirstSearch * currentSearch, double time_spent, 
					      const vector<UCTNode *> & uct_trace){
    Timer t_notify;
    cout << "["  << g_timer() << "] Finished " << ((currentSearch->isFW()? "fw" : "bw"))  << " in " << *(currentSearch->getStateSpace())<< ": " << flush;
    if (!currentSearch->foundSolution()){
	cout << "proved task unsolvable!" << endl;
	generateUnsolvabilityProof(currentSearch);
	statistics(); 
	utils::exit_with(utils::ExitCode::UNSOLVABLE); 
    } else {
	double multiplier = 1.0;
	BDD newDeadEnds = currentSearch->getUnreachableStates();
	//cout << " deadends " <<  vars->percentageNumStates(newDeadEnds) << flush; 
	try {
	    
	    newDeadEnds = tree->getRoot()->getMgr()->filter_mutex(newDeadEnds, !currentSearch->isFW(), 1000000, true);
	    // cout << "  removed:  " << newDeadEnds.nodeCount() << endl;
	    
	} catch(BDDError e) {
	    cout << "  could not remove other dead ends. " << flush;
	    multiplier = 0.1; //Reduce reward by a factor of 10
	}

	newDeadEnds = newDeadEnds*vars->validStates();
	    
       	tree->getRoot()->removeSubsets(currentSearch->getStateSpace()->getFullVars(), currentSearch->isFW());
	
	if (!newDeadEnds.IsZero()) {
	    cout << "  found dead ends: " << newDeadEnds.nodeCount() << endl; 

	    if(insert_dead_ends) {
		insertDeadEnds(newDeadEnds, currentSearch->isFW());
	    }
	    num_iterations_without_reward = 0;
	    time_last_reward = g_timer();
	} else {
	    cout <<  "  with no results "  << endl; 
	    increase_num_iterations_without_reward();
	}

	double reward = computeReward(newDeadEnds, time_spent)*multiplier;
	DEBUG_MSG(cout << "Reward: " << reward << endl;);

	for (UCTNode * node : uct_trace) {
	    if(RAVE_K) {
		node->notifyRewardRAVE(currentSearch->isFW(), reward, uct_trace.back()->getPattern());
	    }else{
		node->notifyReward(currentSearch->isFW(), reward);
	    }	    
	}
    }
    
    time_notify_mutex += t_notify();
} 


double SymPA::computeReward (const BDD & bdd, double time_spent) const {
    if(bdd.IsZero()) return 0;
    switch(rewardType) {
    case STATES_NODES:
	if (bdd.nodeCount() < 100000) {
	    return std::max<double>(1.0, 10*vars->percentageNumStates(bdd));
	} else {
	    return std::max <double> (0.25, (10000000 - bdd.nodeCount())/10000000.0);
	}
	
    case STATES: 
	return vars->percentageNumStates(bdd);
    case NODES:
	return  std::min(std::max <double> (0.5, bdd.nodeCount()/100000.0), 10.0);
    case STATES_TIME:
	return vars->percentageNumStates(bdd) * 1800.0/(1 + time_spent); 
    case NODES_TIME:
	return (bdd.nodeCount()/100000.0) * 1800.0/(1+ time_spent);
    case NONE: 
    case RAND:
	return 0;
    }
    return 0;
} 

BreadthFirstSearch * SymPA::selectExploration() {
    //DEBUG_PHPDBS(cout << "We have " << ongoing_searches.size() << " ongoing_searches" << endl;);
    //1) Look in already generated explorations => get the easiest one
    //(gives preference to shouldSearch abstractions)
    std::sort(ongoing_searches.begin(), ongoing_searches.end(),
    	      [this] (BreadthFirstSearch * e1, 
		      BreadthFirstSearch * e2){
    		  return e1->isBetter (*e2); 
    	      });

    for(auto exp : ongoing_searches){
	if(exp->isSearchable()){
	    return exp;
	}
    }
    
    //3) Ask hierarchy policies to generate new heuristics/explorations
    if (askHeuristic()) return nullptr;
    else return ongoing_searches.front(); 
}


bool SymPA::chooseDirection() const {
    if (abstractDir ==  Dir::FW) {
	return  true;
    }else if (abstractDir == Dir::BW) {
	return false;	
    }

    return tree->getRoot()->chooseDirection(UCT_C);
}

bool SymPA::askHeuristic() {
    Timer t_gen_heuristic;
    DEBUG_MSG(cout << "Ask heuristic" << endl;);
    // cout << *tree << endl;
    
    while(t_gen_heuristic() < phTime && 
	  vars->totalMemory() < phMemory &&
	  numAbstractions < maxNumAbstractions){	
	numAbstractions++;

	vector<UCTNode *> uct_trace;
	uct_trace.push_back(tree->getRoot());

	bool fw = chooseDirection();
	    
	//1) Generate a new abstract exploration
	UCTNode * abstractNode = relax(tree->getRoot(), fw, uct_trace, false);
	if(!abstractNode) {
	    increase_num_iterations_without_reward(); 
	    if(RAVE_K) for (auto node : uct_trace) node->notifyRewardRAVE(fw, 0, uct_trace.back()->getPattern());
	    else for (auto node : uct_trace) node->notifyReward(fw, 0);
	    continue;
	}

	BreadthFirstSearch * abstractExp = abstractNode->getSearch(fw);
	//2) Search the new exploration
	while(abstractExp &&
	      !abstractExp->finished() &&
	      vars->totalMemory() < phMemory && 
	      t_gen_heuristic() < phTime){
	    if (abstractExp->isSearchable()){
		Timer t_step;
		abstractExp->step();
		time_step_abstract += t_step();
		
		
		if(abstractExp->finished()) {
		    DEBUG_MSG(cout << "Abstract search finished" << endl;);
		    notifyFinishedAbstractSearch(abstractExp, t_gen_heuristic(), uct_trace); 
		    return true;
		}
	    } else {
		DEBUG_MSG(cout << "Abstract search not searchable" << endl;);

		bool override_search =  g_timer() < add_abstract_to_ongoing_searches_time;
		
		if(!override_search) {
		    ongoing_searches.push_back(abstractExp); //Store ongoing searches
		} 
		//If we cannot continue the search, we relax it even more
		abstractNode = relax(abstractNode, fw, uct_trace, override_search);

		    
		if(!abstractNode){
		    increase_num_iterations_without_reward();
		    if (RAVE_K) for (auto node : uct_trace) node->notifyRewardRAVE(fw, 0, uct_trace.back()->getPattern());
		    else for (auto node : uct_trace) node->notifyReward(fw, 0);
		    return true;
		}
		abstractExp = abstractNode->getSearch(fw);
	    }
	}
    }
    
    //I did not generate any heuristic
    return false;
}
/*
 * Generates three files which proof that the task is unsolvable.
 * The three files are:
 * - the closed list as BDD,
 * - the task translated to a format suitable for the verifier,
 * - the actual proof in a format suitable for the verifier.
 * This proof can then be verified by the verifier from
 * PhD thesis "Certifying Planning Systems: Witnesses for Unsolvability"
 * by Salome Eriksson, 2019
 */
void SymPA::generateUnsolvabilityProof(BreadthFirstSearch *currentSearch) {
    // only generate unsolvability proof if search finished without finding a solution
    assert(currentSearch->finished() && !currentSearch->foundSolution());
    cout << "Begin writing unsolvability certificate...\n";
    Timer timer;

    std::string dir;
    if (getenv("TMPDIR")) {
        dir = ((std::string) getenv("TMPDIR")) + "/";
    } else {
        dir = "./";
    }
    std::string bdd_filename = dir + "search.bdd";
    std::string proof_filename = dir + "proof.txt";
    std::string task_filename = dir + "task.txt";
    if (currentSearch->isFW()) {
        writeForwardUnsolvabilityProof(proof_filename, bdd_filename);
    } else {
        writeBackwardUnsolvabilityProof(proof_filename, bdd_filename);
    }
    writeBDDsForUnsolvabilityProof(currentSearch, bdd_filename);
    if (!writeTaskFileForUnsolvabilityProof(task_filename)) {
        cout << "Finished writing unsolvability certificate.\n";
    } else {
        cout << "Could not generate task file.\n";
        std::remove(task_filename.c_str());
        std::remove(proof_filename.c_str());
        std::remove(bdd_filename.c_str());
    }
    double cert_gen_time = timer();
    cout << "Certificate generation time: " << cert_gen_time << "s\n";
}

/*
 * Writes the forward unsolvability proof
 * in a file.
 */
    void SymPA::writeForwardUnsolvabilityProof(std::string proof_filename, std::string bdd_filename) {
        DEBUG_MSG(cout << "Start writing forward proof to file." << endl;);
        int setcount = 0;
        int knowledgecount = 0;

        std::ofstream stream;
        stream.open(proof_filename);

        int emptysetid = setcount++;
        stream << "e " << emptysetid << " c e\n";
        int goalsetid = setcount++;
        stream << "e " << goalsetid << " c g\n";
        int initsetid = setcount++;
        stream << "e " << initsetid << " c i\n";
        int k_empty_dead = knowledgecount++;
        stream << "k " << k_empty_dead << " d " << emptysetid << " d1\n";  // judgement (1)
        stream << "a 0 a\n";

        int closedStates = setcount++;
        stream << "e " << closedStates << " b " << bdd_filename << " "
                   << 0 << " ;\n";
        // 0 because bdd of closed list is first bdd in bdd file

        if (!dead_end_fw.empty()) {

            int deadEnds = setcount++;
            stream << "e " << deadEnds << " b " << bdd_filename << " "
                   << 1 << " ;\n";
            // 1 because bdd file contains only bdd for closed states (0) and bdd for dead ends (1)

            int intersection_deadEnds_goal = setcount++;
            stream << "e " << intersection_deadEnds_goal << " i "
                   << deadEnds << " " << goalsetid << "\n";

            int k_intersection_deadEnds_goal_empty = knowledgecount++;
            stream << "k " << k_intersection_deadEnds_goal_empty << " s "
                   << intersection_deadEnds_goal << " " << emptysetid << " b1\n";  // judgement (2)

            int k_intersection_deadEnds_goal_dead = knowledgecount++;
            stream << "k " << k_intersection_deadEnds_goal_dead << " d " << intersection_deadEnds_goal
                       << " d3 " << k_intersection_deadEnds_goal_empty << " " << k_empty_dead << "\n";  // judgement (3)

            int deadEnds_progression = setcount++;
            stream << "e " << deadEnds_progression << " p " << deadEnds << " 0\n";
            // apparently the 0 at the end is needed for some reasons
            int union_deadEnds_emptySet = setcount++;
            stream << "e " << union_deadEnds_emptySet << " u "
                   << deadEnds << " " << emptysetid << "\n";
            int k_deadEnds_progression_subset_union_deadEnds_emptySet = knowledgecount++;
            stream << "k " << k_deadEnds_progression_subset_union_deadEnds_emptySet << " s "
                   << deadEnds_progression << " " << union_deadEnds_emptySet << " b2\n";  // judgement (4)

            int k_deadEnds_dead = knowledgecount++;
            stream << "k " << k_deadEnds_dead << " d " << deadEnds << " d6 "
                   << k_deadEnds_progression_subset_union_deadEnds_emptySet << " "
                   << k_empty_dead << " " << k_intersection_deadEnds_goal_dead << "\n";  // judgement (5)

            int intersection_closedStates_goal = setcount++;
            stream << "e " << intersection_closedStates_goal << " i "
                   << closedStates << " " << goalsetid << "\n";
            int k_intersection_closedStates_goal_empty = knowledgecount++;
            stream << "k " << k_intersection_closedStates_goal_empty << " s "
                   << intersection_closedStates_goal << " " << emptysetid << " b1\n";  // judgement (6)

            int k_intersection_closedStates_goal_dead = knowledgecount++;
            stream << "k " << k_intersection_closedStates_goal_dead << " d " << intersection_closedStates_goal
                       << " d3 " << k_intersection_closedStates_goal_empty << " " << k_empty_dead << "\n";  // judgement (7)

            int closedStates_progression = setcount++;
            stream << "e " << closedStates_progression << " p " << closedStates << " 0\n";
            // apparently the 0 at the end is needed for some reasons
            int union_closedStates_deadEnds = setcount++;
            stream << "e " << union_closedStates_deadEnds << " u "
                   << closedStates << " " << deadEnds << "\n";
            int k_closedStates_progression_subset_union_closedStates_deadEnds = knowledgecount++;
            stream << "k " << k_closedStates_progression_subset_union_closedStates_deadEnds << " s "
                   << closedStates_progression << " " << union_closedStates_deadEnds << " b2\n";  // judgement (8)

            int k_closedStates_dead = knowledgecount++;
            stream << "k " << k_closedStates_dead << " d " << closedStates << " d6 "
                   << k_closedStates_progression_subset_union_closedStates_deadEnds << " "
                   << k_deadEnds_dead << " " << k_intersection_closedStates_goal_dead << "\n";  // judgement (9)

            int k_init_in_closedStates = knowledgecount++;
            stream << "k " << k_init_in_closedStates << " s "
                       << initsetid << " " << closedStates << " b1\n";  // judgement (10)
            int k_init_dead = knowledgecount++;
            stream << "k " << k_init_dead << " d " << initsetid << " d3 "
                       << k_init_in_closedStates << " " << k_closedStates_dead << "\n";  // judgement (11)
            stream << "k " << knowledgecount++ << " u d4 " << k_init_dead << "\n";  // judgement (12)

        } else {  // no dead ends were found

            // the following is almost the same as above beginning at judgement (6)

            int intersection_closedStates_goal = setcount++;
            stream << "e " << intersection_closedStates_goal << " i "
                   << closedStates << " " << goalsetid << "\n";
            int k_intersection_closedStates_goal_empty = knowledgecount++;
            stream << "k " << k_intersection_closedStates_goal_empty << " s "
                   << intersection_closedStates_goal << " " << emptysetid << " b1\n";  // judgement (6)

            int k_intersection_closedStates_goal_dead = knowledgecount++;
            stream << "k " << k_intersection_closedStates_goal_dead << " d " << intersection_closedStates_goal
                       << " d3 " << k_intersection_closedStates_goal_empty << " " << k_empty_dead << "\n";  // judgement (7)

            int closedStates_progression = setcount++;
            stream << "e " << closedStates_progression << " p " << closedStates << " 0\n";
            // apparently the 0 at the end is needed for some reasons
            int union_closedStates_emptySet = setcount++;
            stream << "e " << union_closedStates_emptySet << " u "
                   << closedStates << " " << emptysetid << "\n";
            int k_closedStates_progression_subset_union_closedStates_emptySet = knowledgecount++;
            stream << "k " << k_closedStates_progression_subset_union_closedStates_emptySet << " s "
                   << closedStates_progression << " " << union_closedStates_emptySet << " b2\n";  // modified judgement (8)

            int k_closedStates_dead = knowledgecount++;
            stream << "k " << k_closedStates_dead << " d " << closedStates << " d6 "
                   << k_closedStates_progression_subset_union_closedStates_emptySet << " "
                   << k_empty_dead << " " << k_intersection_closedStates_goal_dead << "\n";  // judgement (9)

            int k_init_in_closedStates = knowledgecount++;
            stream << "k " << k_init_in_closedStates << " s "
                       << initsetid << " " << closedStates << " b1\n";  // judgement (10)
            int k_init_dead = knowledgecount++;
            stream << "k " << k_init_dead << " d " << initsetid << " d3 "
                       << k_init_in_closedStates << " " << k_closedStates_dead << "\n";  // judgement (11)
            stream << "k " << knowledgecount++ << " u d4 " << k_init_dead << "\n";  // judgement (12)
        }

        stream.close();
        DEBUG_MSG(cout << "Finished writing forward proof to file." << endl;);
    }

/*
 * Writes the backward unsolvability proof
 * in a file.
 */
    void SymPA::writeBackwardUnsolvabilityProof(std::string proof_filename, std::string bdd_filename) {
        DEBUG_MSG(cout << "Start writing backward proof to file." << endl;);
        int setcount = 0;
        int knowledgecount = 0;

        std::ofstream stream;
        stream.open(proof_filename);

        int emptysetid = setcount++;
        stream << "e " << emptysetid << " c e\n";
        int goalsetid = setcount++;
        stream << "e " << goalsetid << " c g\n";
        int initsetid = setcount++;
        stream << "e " << initsetid << " c i\n";
        int k_empty_dead = knowledgecount++;
        stream << "k " << k_empty_dead << " d " << emptysetid << " d1\n";  // judgement (1)
        stream << "a 0 a\n";

        int closedStates = setcount++;
        stream << "e " << closedStates << " b " << bdd_filename << " "
               << 0 << " ;\n";
        // 0 because bdd of closed list is first bdd in bdd file

        if (!dead_end_fw.empty()) {

            int deadEnds = setcount++;
            stream << "e " << deadEnds << " b " << bdd_filename << " "
                   << 1 << " ;\n";
            // 1 because bdd file contains only bdd for closed states (0) and bdd for dead ends (1)

            int deadEnds_regression = setcount++;
            stream << "e " << deadEnds_regression << " r " << deadEnds << " 0\n";
            // apparently the 0 at the end is needed for some reasons
            int union_deadEnds_emptySet = setcount++;
            stream << "e " << union_deadEnds_emptySet << " u "
                   << deadEnds << " " << emptysetid << "\n";
            int k_deadEnds_regression_subset_union_deadEnds_emptySet = knowledgecount++;
            stream << "k " << k_deadEnds_regression_subset_union_deadEnds_emptySet << " s "
                   << deadEnds_regression << " " << union_deadEnds_emptySet << " b3\n";  // judgement (2)

            int neg_deadEnds = setcount++;
            stream << "e " << neg_deadEnds << " n " << deadEnds << "\n";
            int k_init_in_neg_deadEnds = knowledgecount++;
            stream << "k " << k_init_in_neg_deadEnds << " s "
                   << initsetid << " " << neg_deadEnds << " b1\n";  // judgement (3)

            int k_deadEnds_dead = knowledgecount++;
            stream << "k " << k_deadEnds_dead << " d " << deadEnds << " d9 "
                   << k_deadEnds_regression_subset_union_deadEnds_emptySet << " "
                   << k_empty_dead << " " << k_init_in_neg_deadEnds << "\n";  // judgement (4)

            int closedStates_regression = setcount++;
            stream << "e " << closedStates_regression << " r " << closedStates << " 0\n";
            // apparently the 0 at the end is needed for some reasons
            int union_closedStates_deadEnds = setcount++;
            stream << "e " << union_closedStates_deadEnds << " u "
                   << closedStates << " " << deadEnds << "\n";
            int k_closedStates_regression_subset_union_closedStates_deadEnds = knowledgecount++;
            stream << "k " << k_closedStates_regression_subset_union_closedStates_deadEnds << " s "
                   << closedStates_regression << " " << union_closedStates_deadEnds << " b3\n";  // judgement (5)

            int neg_closedStates = setcount++;
            stream << "e " << neg_closedStates << " n " << closedStates << "\n";
            int k_init_in_neg_closedStates = knowledgecount++;
            stream << "k " << k_init_in_neg_closedStates << " s "
                   << initsetid << " " << neg_closedStates << " b1\n";  // judgement (6)

            int k_closedStates_dead = knowledgecount++;
            stream << "k " << k_closedStates_dead << " d " << closedStates << " d9 "
                   << k_closedStates_regression_subset_union_closedStates_deadEnds << " "
                   << k_deadEnds_dead << " " << k_init_in_neg_closedStates << "\n";  // judgement (7)

            int k_goals_in_closedStates = knowledgecount++;
            stream << "k " << k_goals_in_closedStates << " s "
                   << goalsetid << " " << closedStates << " b1\n";  // judgement (8)

            int k_goals_dead = knowledgecount++;
            stream << "k " << k_goals_dead << " d " << goalsetid << " d3 "
                   << k_goals_in_closedStates << " " << k_closedStates_dead << "\n";  // judgement (9)
            stream << "k " << knowledgecount++ << " u d5 " << k_goals_dead << "\n";  // judgement (10)

        } else {  // no dead ends were found

            int closedStates_regression = setcount++;
            stream << "e " << closedStates_regression << " r " << closedStates << " 0\n";
            // apparently the 0 at the end is needed for some reasons
            int union_closedStates_emptySet = setcount++;
            stream << "e " << union_closedStates_emptySet << " u "
                   << closedStates << " " << emptysetid << "\n";
            int k_closedStates_regression_subset_union_closedStates_emptySet = knowledgecount++;
            stream << "k " << k_closedStates_regression_subset_union_closedStates_emptySet << " s "
                   << closedStates_regression << " " << union_closedStates_emptySet << " b3\n";
                   // modified judgement (5)

            int neg_closedStates = setcount++;
            stream << "e " << neg_closedStates << " n " << closedStates << "\n";
            int k_init_in_neg_closedStates = knowledgecount++;
            stream << "k " << k_init_in_neg_closedStates << " s "
                   << initsetid << " " << neg_closedStates << " b1\n";  // judgement (6)

            int k_closedStates_dead = knowledgecount++;
            stream << "k " << k_closedStates_dead << " d " << closedStates << " d9 "
                   << k_closedStates_regression_subset_union_closedStates_emptySet << " "
                   << k_empty_dead << " " << k_init_in_neg_closedStates << "\n";  // judgement (7)

            int k_goals_in_closedStates = knowledgecount++;
            stream << "k " << k_goals_in_closedStates << " s "
                   << goalsetid << " " << closedStates << " b1\n";  // judgement (8)

            int k_goals_dead = knowledgecount++;
            stream << "k " << k_goals_dead << " d " << goalsetid << " d3 "
                   << k_goals_in_closedStates << " " << k_closedStates_dead << "\n";  // judgement (9)
            stream << "k " << knowledgecount++ << " u d5 " << k_goals_dead << "\n";  // judgement (10)

        }

        stream.close();
        DEBUG_MSG(cout << "Finished writing backward proof to file." << endl;);

    }

/*
 * Writes the closed list BDD to a file which contributes to
 * the unsolvability proof.
 */
void SymPA::writeBDDsForUnsolvabilityProof(BreadthFirstSearch *currentSearch, std::string bdd_filename) {
    DEBUG_MSG(cout << "Start writing BDDs to file." << endl;);
    SymVariables *symVars = getVars();
    std::ofstream stream;

    // first line: variables of the BDDs
    // remark: variable order is assumed to be fix for all BDDs
    stream.open(bdd_filename);
    for(int var : symVars->getVarOrder()) {
        std::vector<int> bddVarsIndex = symVars->vars_index_pre(var);
        std::vector<int> bddPrimedVarsIndex = symVars->vars_index_eff(var);
        for(int i = 0; i < (int) bddVarsIndex.size(); ++i) {
            stream << bddVarsIndex[i] << " ";
            stream << bddPrimedVarsIndex[i] << " ";
        }
    }
    stream << "\n";
    stream.close();

    std::vector<BDD> bdds;
    BDD closedStates = currentSearch->getAllClosedStates();
    bdds.push_back(closedStates);
    if (currentSearch->isFW()) {
        if (!dead_end_fw.empty()) {
            BDD totalDeadEndsFW = symVars->zeroBDD();
            for (BDD deadEnd : dead_end_fw) {
                totalDeadEndsFW += deadEnd;
            }
            bdds.push_back(totalDeadEndsFW);
        }
        // for (BDD deadEnd : dead_end_fw) {
        //     bdds.push_back(deadEnd);
        // }
    } else {
        if (!dead_end_bw.empty()) {
            BDD totalDeadEndsBW = symVars->zeroBDD();
            for (BDD deadEnd : dead_end_bw) {
                totalDeadEndsBW += deadEnd;
            }
            bdds.push_back(totalDeadEndsBW);
        }
        // for (BDD deadEnd : dead_end_bw) {
        //     bdds.push_back(deadEnd);
        // }
    }
    int numBDDs = bdds.size();

    FILE *fp;
    fp = fopen(bdd_filename.c_str(), "a");
    DdNode** bdd_arr = new DdNode*[numBDDs];
    for(int i = 0; i < numBDDs; ++i) {
        // second line: enumeration over how many BDDs will be in file
        fprintf (fp, "%d ",i);
        // fill array with BDDs that should be written to file
        bdd_arr[i] = bdds[i].getNode();
    }
    fprintf (fp, "\n");

    Cudd *mgr = symVars->mgr();
    DdManager *ddmgr = mgr->getManager();

    /*
    BDD toPrint = currentSearch->mgr->getInitialState();
    FILE *outfile; // output file pointer for .dot file
    outfile = fopen("sympa-bdd.dot","w");
    DdNode **ddnodearray = (DdNode**)malloc(sizeof(DdNode*)); // initialize the function array
    ddnodearray[0] = toPrint.getNode();
    DdManager *printmgr = toPrint.manager();
    Cudd_DumpDot(printmgr, 1, ddnodearray, NULL, NULL, outfile); // dump the function to .dot file
    free(ddnodearray);
    fclose (outfile); // close the file
    */

    Dddmp_cuddBddArrayStore(ddmgr, NULL, numBDDs, &bdd_arr[0], NULL,
                            NULL, NULL, DDDMP_MODE_TEXT, DDDMP_VARDEFAULT, NULL, fp);
    DEBUG_MSG(cout << "Finished writing BDDs to file." << endl;);
}

/*
 * Writes the task in a specific format to a file
 * such that the unsolvability proof can be
 * verified by above mentioned verifier.
 */
int SymPA::writeTaskFileForUnsolvabilityProof(std::string task_filename) {
    DEBUG_MSG(cout << "Start writing task file." << endl;);

    SymVariables *symVars = getVars();

    std::ofstream stream;
    stream.open(task_filename);

    // *2 because the BDD contains for every binary variable a "primed" variable
    // these primed variables are needed although they will not be used
    int numAtoms = symVars->getNumBDDVars() * 2;

    stream << "begin_atoms:" << numAtoms << "\n";
    for(int var : symVars->getVarOrder()) {
        std::vector<int> bddVarsIndex = symVars->vars_index_pre(var);
        std::vector<int> bddPrimedVarsIndex = symVars->vars_index_eff(var);
        for(int i = 0; i < (int) bddVarsIndex.size(); ++i) {
            stream << bddVarsIndex[i] << "\n";
            stream << bddPrimedVarsIndex[i] << "\n";
        }
    }
    stream << "end_atoms\n";

    stream << "begin_init\n";
    for (int var = 0; var < (int) task_proxy.get_variables().size(); ++var) {
        assert(task_proxy.get_initial_state()[var].get_variable().get_id() == var);
        //cout << "sas var: " << task_proxy.get_initial_state()[var].get_variable().get_id() << " " << task_proxy.get_initial_state()[var].get_variable().get_name() << ", val: " << task_proxy.get_initial_state()[var].get_value() << "\n";
        std::vector<pair<int, bool>> vec =
                getBinaryVariablesWithValues(var, task_proxy.get_initial_state()[var].get_value());
        for (pair<int, bool> p : vec) {
            //cout << " bin var " << p.first << ", val: " << p.second << "\n";
            if (p.second) {
                //stream << p.first * 2 << "\n";  // *2 to skip primed variables
                stream << p.first << "\n";
            }
        }
    }
    stream << "end_init\n";

    stream << "begin_goal\n";
    std::vector<pair<int, bool>> posBinGoals;
    std::vector<pair<int, bool>> negBinGoals;
    for (int i = 0; i < (int) task_proxy.get_goals().size(); ++i) {
        int id = task_proxy.get_goals()[i].get_variable().get_id();
        int val = task_proxy.get_goals()[i].get_value();
        //cout << "goal id: " << id << ", val: " << val << "\n";
        std::vector<pair<int, bool>> vec = getBinaryVariablesWithValues(id, val);
        for (pair<int, bool> p : vec) {
            //cout << "bin id: " << p.first << ", val: " << p.second << "\n";
            if (p.second) {
                posBinGoals.push_back(p);
            } else {
                negBinGoals.push_back(p);
            }
        }
    }
    stream << "begin_pos\n";
    for (pair<int, bool> p : posBinGoals) {
        //stream << p.first * 2 << "\n";  // *2 to skip primed variables
        stream << p.first << "\n";
    }
    stream << "end_pos\n";
    stream << "begin_neg\n";
    for (pair<int, bool> p : negBinGoals) {
        //stream << p.first * 2 << "\n";  // *2 to skip primed variables
        stream << p.first << "\n";
    }
    stream << "end_neg\n";
    stream << "end_goal\n";

    int numActions = task_proxy.get_operators().size();
    stream << "begin_actions:" << numActions << "\n";
    for (int actionId = 0; actionId < numActions; ++actionId) {
        stream << "begin_action\n";
        OperatorProxy action = task_proxy.get_operators()[actionId];
        stream << action.get_name() << "\n";
        stream << "cost: " << action.get_cost() << "\n";

        std::vector<pair<int, bool>> posPre;
        std::vector<pair<int, bool>> negPre;
        for (int i = 0; i < (int) action.get_preconditions().size(); ++i) {
            int id = action.get_preconditions()[i].get_variable().get_id();
            int val = action.get_preconditions()[i].get_value();
            std::vector<pair<int, bool>> vec = getBinaryVariablesWithValues(id, val);
            for (pair<int, bool> p : vec) {
                if (p.second) {
                    posPre.push_back(p);
                } else {
                    negPre.push_back(p);
                }
            }
        }
        for (pair<int, bool> p : posPre) {
            //stream << "PRE:" << p.first * 2 << "\n";  // *2 to skip primed variables
            stream << "PRE:" << p.first << "\n";
        }
        for (pair<int, bool> p : negPre) {
            //stream << "PRE-:" << p.first * 2 << "\n";  // *2 to skip primed variables
            stream << "PRE-:" << p.first << "\n";
        }

        std::vector<pair<int, bool>> add;
        std::vector<pair<int, bool>> del;
        for (int i = 0; i < (int) action.get_effects().size(); ++i) {
            if(!action.get_effects()[i].get_conditions().empty()) {
                cout << "CONDITIONAL EFFECTS, abort certificate generation!\n";
                stream.close();
                return 1;

            }
            int id = action.get_effects()[i].get_fact().get_variable().get_id();
            int val = action.get_effects()[i].get_fact().get_value();
            std::vector<pair<int, bool>> vec = getBinaryVariablesWithValues(id, val);
            for (pair<int, bool> p : vec) {
                if (p.second) {
                    add.push_back(p);
                } else {
                    del.push_back(p);
                }
            }
        }
        for (pair<int, bool> p : add) {
            //stream << "ADD:" << p.first * 2 << "\n";  // *2 to skip primed variables
            stream << "ADD:" << p.first << "\n";
        }
        for (pair<int, bool> p : del) {
            //stream << "DEL:" << p.first * 2 << "\n";  // *2 to skip primed variables
            stream << "DEL:" << p.first << "\n";
        }
        stream << "end_action\n";
    }
    stream << "end_actions\n";

    stream.close();
    DEBUG_MSG(cout << "Finished writing task file." << endl;);
    return 0;
}

int SymPA::getNumberOfBinaryVariables(int variableId) {
    // counts bits of domain size because this tells
    // how many binary variables this variable uses
    if (task_proxy.get_variables()[variableId].get_domain_size() < 2) {
        return 1;
    } else {
        return ceil(log2(task_proxy.get_variables()[variableId].get_domain_size()));
    }

}

// returns index and value of binary variables corresponding to the given id and value of a normal variable
std::vector< std::pair<int, bool> > SymPA::getBinaryVariablesWithValues(int variableId, int variableValue) {

    std::vector< std::pair<int, bool> > binVarIndexAndBinVarVal;
    SymVariables* symVars = getVars();
    std::vector<int> var_order = symVars->getVarOrder();

    for (int v : var_order) {
        if (v == variableId) {
            for (size_t j = 0; j < symVars->vars_index_pre(v).size(); j++) {
                binVarIndexAndBinVarVal.push_back(make_pair(symVars->vars_index_pre(v)[j], ((variableValue >> j) % 2)));
            }
        }
    }

    /*
    int pos = 0;
    for (int v : var_order) {
        for (size_t j = 0; j < symVars->vars_index_pre(v).size(); j++) {
            //cout << "pos: " << pos << "  j: " << j << "  v: " << v << "  varidableId: " << variableId << "\n";
            if (v == variableId) {
                binVarIndexAndBinVarVal.push_back(make_pair(pos, ((variableValue >> j) % 2)));
            }
            pos += 2; //Skip interleaving variable
        }
    }
    */

    /*
    // skip variables before variable "variableId"
    int indexBinVars = 0;
    int symVarId = 0;
    while (var_order[symVarId] != variableId) {
        indexBinVars += getNumberOfBinaryVariables(symVarId);
        symVarId++;
    }
    //for (int i = 0; i <= variableId; ++i) {
    //    indexBinVars += getNumberOfBinaryVariables(i);
    //}

    int vecIndex = 0;
    // iterate through the binary variables of the variable "variableId"
    for (int i = indexBinVars - getNumberOfBinaryVariables(variableId); i < indexBinVars ; ++i) {
        if (variableValue % 2) {
            binVarIndexAndBinVarVal[vecIndex] = make_pair(i, true);
        } else {
            binVarIndexAndBinVarVal[vecIndex] = make_pair(i, false);
        }
        variableValue = variableValue / 2;
        vecIndex++;
    }
    */
        return binVarIndexAndBinVarVal;
}

void SymPA::print_options() const{
    cout << "SymPA* " << endl;
    cout << "   Search dir: " << searchDir << endl;
    cout << "   Abstract dir: " << abstractDir << endl;

    cout << "  Max num abstractions: " << maxNumAbstractions << endl;
    cout << "   Abs TRs Strategy: " << absTRsStrategy << endl;
    cout << "   PH time: " << phTime << ", memory: " << phMemory << endl;
    cout << "   Relax time: " << maxRelaxTime << ", nodes: " << maxRelaxNodes << endl;
    cout << "   Ratio relax time: " <<  ratioRelaxTime << ", nodes: " << ratioRelaxNodes << endl;
    cout << "   Perimeter Abstractions: " << (perimeterPDBs ? "yes" : "no") << endl;
    cout << "   ShouldAbstract ratio: " << shouldAbstractRatio << endl;

    mgrParams.print_options();
    searchParams.print_options();
}

void SymPA::statistics() const{
    cout << endl << "Total BDD Nodes: " << vars->totalNodes() << endl;
    cout << "Initialization time: " << time_init << endl;
    cout << "Total time spent in original search: " << time_step_original << "s" << endl;
    cout << "Total time spent in abstract searches: " << time_step_abstract<<"s" <<   endl;
    cout << "Total time spent relaxing: " << time_select_exploration << "s" << endl;
    cout << "Total time spent notifying mutexes: " << time_notify_mutex << "s" << endl;

    cout << "Total time: " << g_timer() << "s" << endl;

}

static SearchEngine *_parse(OptionParser &parser) {

    SearchEngine::add_options_to_parser(parser);
    SymController::add_options_to_parser(parser, 45e3, 1e7);

    parser.add_enum_option("search_dir", DirValues,
			   "search direction", "BIDIR");

    parser.add_enum_option("abstract_dir", DirValues,
			   "search direction in abstract searches", "BIDIR");

    SymParamsMgr::add_options_to_parser(parser);
    SymParamsSearch::add_options_to_parser(parser, 30e3, 1e7);

    parser.add_option<int>("max_abstractions",
			   "maximum number of calls to askHeuristic", "100000");

    parser.add_option<double>("ph_time", 
			      "allowed time to use the ph", "500");
    parser.add_option<double>("ph_memory",
			      "allowed memory to use the ph", to_string(3.0e9));

    parser.add_option<int>("max_relax_time",
			   "allowed time to relax the search", to_string(10e3));
    parser.add_option<int>("max_relax_nodes",
			   "allowed nodes to relax the search", to_string(10e7));
    parser.add_option<double>("relax_ratio_time",
			      "allowed time to accept the abstraction after relaxing the search.", 
			      "0.5");
    parser.add_option<double>("relax_ratio_nodes",
			      "allowed nodes to accept the abstraction after relaxing the search.", "0.5");


    parser.add_option<double>("multiply_time_by",
			      "", 
			      "1.5");

    parser.add_option<int>("num_fails_to_multiply_time",
			   "", "10");

    parser.add_option<int>("time_fails_to_multiply_time",
			   "", "100");

      
    parser.add_enum_option("tr_st", AbsTRsStrategyValues,
			   "abstraction TRs strategy", "IND_TR_SHRINK");
    
    parser.add_option<bool>("insert_dead_ends",  "uses dead end states to prune other searches", "true");

    parser.add_option<bool>("perimeter_pdbs",  "initializes explorations with the one being relaxed", "true");

    parser.add_option<double>("should_abstract_ratio",
			      "relax the search when has more than this estimated time/nodes· If it is zero, it abstract the current perimeter (when askHeuristic is called)", "0");

    parser.add_option<double>("ratio_increase", 
			      "maxStepTime is multiplied by ratio to the number of abstractions", "1.5");

    parser.add_option<double>("uct_c", 
			      "constant for uct formula", "1.0");

    parser.add_option<double>("rave_k", 
			      "constant for RAVE formula. Disabled by default", "0.0");

    parser.add_enum_option("reward_type", UCTRewardTypeValues,
			   "type of reward function", "RANDOM");

    parser.add_option<int>("add_abstract_to_ongoing_searches_time", 
			    "includes the abstract searches that were incomplete to the set of ongoing searches", 
			    "0");

    Options opts = parser.parse();

    SearchEngine *policy = 0;
    if (!parser.dry_run()) {
	policy = new SymPA(opts);
    }  
    return policy;
}

static Plugin<SearchEngine> _plugin("sympa", _parse);

}
