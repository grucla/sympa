#ifndef SYMBOLIC_UCT_PDBS_H
#define SYMBOLIC_UCT_PDBS_H

#include "../option_parser.h"
#include "../operator_cost.h"
#include "sym_enums.h" 
#include "sym_pdb.h"

namespace symbolic {

    class BreadthFirstSearch;
    class SymParamsSearch;
    class SymStateSpaceManager;

    class UCTInfo {
    public:
	double reward;
	int visits;
	bool redundant;	

	UCTInfo() : reward (0), visits(0), redundant(false) {}

	bool explored() const {
	    return visits > 0;
	}

	double uct_value(int visits_parent, double UCT_C, double RAVE_B = 0.0) const {
	    return (1.0-RAVE_B)*reward + UCT_C*sqrt(log(visits_parent)/visits); 
	}

	void update_reward(double new_reward) {
	    reward = (reward * visits + new_reward) / (visits+1);
	    visits++;
    
	}

    }; 

    class UCTTree;


class UCTNode {
private:
    UCTTree * tree;
    int id;
    std::set<int> pattern;

 
    std::shared_ptr<SymStateSpaceManager> mgr;

    // Nodes more abstracted
    std::vector <UCTNode *> children, children_fw, children_bw;
    std::map <UCTNode *, int> children_var; 
    std::shared_ptr<BreadthFirstSearch> fw_search, bw_search;

    UCTInfo info_fw, info_bw;

    std::vector<double> rave_reward;
    std::vector<int> rave_visits;
    int total_rave_visits;

    
    const UCTInfo & get_info(bool fw) const {
	return fw ? info_fw : info_bw; 
    }

    UCTInfo & get_info(bool fw) {
	return fw ? info_fw : info_bw; 
    }


    bool isExplored (bool fw) const {
	return get_info(fw).explored();
    }

    void print_tree(std::ostream &os, bool fw, std::set<int> & printed) const;

public:
    UCTNode(UCTTree * tree, int id, const SymParamsMgr & mgrParams, OperatorCost cost_type); // Constructor for the original state space
    UCTNode(UCTTree * tree, int id, const std::set<int> & pattern_); // Constructor for abstract state space

    UCTNode(const UCTNode & o) = delete;
    UCTNode(UCTNode &&) = default;
    UCTNode & operator=(const UCTNode& ) = delete;
    UCTNode & operator=(UCTNode &&) = default;
    ~UCTNode() = default; 

    void init(std::shared_ptr<SymStateSpaceManager> parent, AbsTRsStrategy absTRsStrategy);

    BreadthFirstSearch * initSearch(bool fw, 
				    const SymParamsSearch & searchParams);

    void initChildren();

    void refine_pattern (const std::set<int> & pattern, 
			 std::vector<std::pair<int, std::set<int>>> & patterns, 
			 int v) const;


    BreadthFirstSearch * relax(std::shared_ptr<BreadthFirstSearch> search, 
				  const SymParamsSearch & searchParams,
				  int maxRelaxTime, int maxRelaxNodes, 
				  double ratioRelaxTime, double ratioRelaxNodes, 
				  bool perimeterPDBs);

    std::shared_ptr<BreadthFirstSearch> retrieveSearch(bool fw, bool delete_search) {
	std::shared_ptr<BreadthFirstSearch> res;
	if (fw){
	    res = fw_search;
	    if (delete_search) fw_search.reset();
	}else {
	    res = bw_search;	   
	    if (delete_search) bw_search.reset(); 
	}
	return res;
    }

    BreadthFirstSearch * getSearch(bool fw){
	if (fw) return fw_search.get();
	else return bw_search.get(); 
    }


    UCTNode * getChild (bool fw, double UCT_C, double RAVE_K, UCTNode * root) const;
    UCTNode * getRandomChild (bool fw);

    double get_rave_value(int var, double UCT_C) const;

    double uct_value(bool fw, int visits_parent, double UCT_C, double RAVE_B = 0) const;

    std::shared_ptr<SymStateSpaceManager> getMgr() const {
	return mgr; 
    }

    bool isAbstractable () const {
	return pattern.size() > 1;
    }

    const std::set<int> & getPattern() const {
	return pattern;
    }

    bool chooseDirection(double UCT_C) const;

    void propagateNewDeadEnds(BDD bdd, bool isFW);

    void notifyReward (bool fw, double numDeadEndsFound);
    void notifyRewardRAVE (bool fw, double numDeadEndsFound, const std::set<int> & pattern);

    bool existsFinishedSuperset(const std::set<int> & pat, 
				std::pair<bool, bool> & redundant, 
				std::set<UCTNode *> & cache);

    bool existsFinishedSuperset(const std::set<int> & pat, 
				std::pair<bool, bool> & redundant) {
	std::set<UCTNode *> cache;
	return existsFinishedSuperset(pat, redundant, cache);
    }


    void removeSubsets(const std::set<int> & pat, bool fw, 
		       std::set<UCTNode *> & cache);

    void removeSubsets(const std::set<int> & pat, bool fw) {
	std::set<UCTNode *> cache;
	removeSubsets(pat, fw, cache);
    }

    bool isSubset (const std::set<int> & p1, const std::set<int> & p2) const;

    friend std::ostream & operator<<(std::ostream &os, const UCTNode & node);
    std::ostream & print_tree(std::ostream &os, bool fw) const;
};


class UCTTree {
    SymVariables * vars;
    CausalGraph * causal_graph;

    std::vector<std::unique_ptr<UCTNode> > nodes;
    std::map<std::set<int>, UCTNode *> nodesByPattern;


public:
    UCTTree (SymVariables * vars_, const SymParamsMgr & mgrParams, OperatorCost cost_type);

    UCTNode * getRoot () const {
	assert(nodes.size());
	return nodes[0].get();
    }

    UCTNode * getUCTNode (const std::set<int> & pattern);

    SymVariables * get_vars () {
	return vars;
    }

    CausalGraph * get_causal_graph() {
	return causal_graph;
    }


    friend std::ostream & operator<<(std::ostream &os, const UCTTree & node);


};

}

#endif
