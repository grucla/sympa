#! /usr/bin/env python3
#-*- coding: utf-8 -*-

import os
from lab.environments import LocalEnvironment, BaselSlurmEnvironment
from lab.tools import get_script_path
from downward.experiment import FastDownwardExperiment
from downward.reports.absolute import AbsoluteReport
from downward.reports.scatter import ScatterPlotReport
from downward.reports.taskwise import TaskwiseReport

def runs_for_time_report(props):
    if ('search_finished' not in props or props['search_finished']==0):
        return False
    if ('search_time' not in props or props['search_time'] is None):
        return False
    if ('finished_certificate' not in props or props['finished_certificate'] == 0):
        return False
    if ('certificate_generation_time' not in props or props['certificate_generation_time'] is None):
        return False
    if ('verify_finished' not in props or props['verify_finished'] == 0):
        return False
    if ('unsolv_total_time' not in props or props['unsolv_total_time'] is None):
        return False
    return True

repo = os.environ["SYMPA_REPO"]
rev = "exp-v10"
env = BaselSlurmEnvironment(partition="infai_1", email="claudia.grundke@unibas.ch")
#env = LocalEnvironment(processes=2)

benchmarks_dir = os.environ["UNSOLVABLE_BENCHMARKS"]
suite = ["bag-barman-uns16", "bag-gripper-uns16", "bag-transport-uns16", "bottleneck-uns16", "cavediving-uns16", "chessboard-pebbling-uns16", "diagnosis-uns16", "document-transfer-uns16", "over-nomystery-uns16", "over-rovers-uns16", "over-tpp-uns16", "pegsol-row5-uns16", "pegsol-uns16", "sliding-tiles-uns16", "tetris-uns16"]
#suite = ["unsolvable-childsnack:childsnack-02-02-05-04.pddl"]
script = os.path.abspath(get_script_path())
script_dir = os.path.dirname(script)
script_base = os.path.splitext(os.path.basename(script))[0]
exp_name = "%s-%s" % (os.path.basename(script_dir), script_base)
data_dir = os.path.join(script_dir, "data", exp_name)

exp = FastDownwardExperiment(path=data_dir, environment=env)
exp.add_suite(benchmarks_dir, suite)

exp.add_algorithm(
    "sympa", repo, rev,
    ["--search", "sympa(max_step_nodes=10000,mutex_type=MUTEX_NOT)"],
    driver_options=["--build", "cgexperiments",
                    "--translate-time-limit", "30m", "--translate-memory-limit","2G",
	            "--search-time-limit", "30m", "--search-memory-limit","2G"],
    build_options=["cgexperiments"],
)

exp.add_resource("run_verifier_script", "run-verifier.sh")
exp.add_command("verify", ["{run_verifier_script}"])

exp.add_parser(exp.EXITCODE_PARSER)
exp.add_parser(exp.TRANSLATOR_PARSER)
exp.add_parser(exp.SINGLE_SEARCH_PARSER)
exp.add_parser(exp.PLANNER_PARSER)
exp.add_parser("myparser.py")
exp.add_parse_again_step()


exp.add_step("build", exp.build)
exp.add_step("start", exp.start_runs)
exp.add_fetcher()

#TODO: add more reports and attributes (https://lab.readthedocs.io/en/stable/downward.reports.html#)
exp.add_report(AbsoluteReport(attributes=["verify_finished", "begin_writing_certificate", "finished_certificate",  "valid_certificate", "invalid_certificate", "search_finished",  "run_dir", "error", "memory", "unsolv_total_time", "unsolv_abort_time"]), outfile=os.path.join(exp.eval_dir, "overview.html"))
exp.add_report(AbsoluteReport(attributes=["verify_finished", "valid_certificate", "invalid_certificate", "verify_timeout", "verify_oom", "certificate_size_kb"], filter_search_finished=1), outfile=os.path.join(exp.eval_dir, "search_finished.html"))
exp.add_report(TaskwiseReport(attributes=["unsolv_is_certificate", "total_time", "search_time", "planner_time", "unsolv_total_time", "unsolv_exit_message", "verify_returncode"]), outfile=os.path.join(exp.eval_dir, "taskwise.html"))
exp.add_report(AbsoluteReport(attributes=["planner_time", "search_time", "total_time", "certificate_generation_time", "unsolv_total_time", "certificate_size_kb"], filter=runs_for_time_report), outfile=os.path.join(exp.eval_dir, "times.html"))

exp.run_steps()

