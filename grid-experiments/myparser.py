#! /usr/bin/env python

import math

from lab.parser import Parser

def search_finished(content, props):
    if(props['error'] == 'unsolvable'):
        props['search_finished'] = 1
    else:
        props['search_finished'] = 0
    return props

def is_cert(content, props):
    if('unsolv_is_certificate' not in props):
        props['unsolv_is_certificate'] = 'unknown'
    elif(props['unsolv_is_certificate'] == 'valid'):
        props['unsolv_is_certificate'] = 'yes'
    elif(props['unsolv_is_certificate'] == 'not valid'):
        props['unsolv_is_certificate'] = 'no'
    return props

def verify_timeout(content, props):
    if('verify_returncode' in props and props['verify_returncode'] == 7):
        props['verify_timeout'] = 1
    else:
        props['verify_timeout'] = 0
    return props

def verify_oom(content, props):
    if('verify_returncode' in props and props['verify_returncode'] == 6):
        props['verify_oom'] = 1
    else:
        props['verify_oom'] = 0
    return props

def verify_finished(content, props):
    if(not props['unsolv_is_certificate'] == 'unknown'):
        props['verify_finished'] = 1
    else:
        props['verify_finished'] = 0 
    return props

def invalid_certificate(content, props):
    if(props['unsolv_is_certificate'] == 'no'):
        props['invalid_certificate'] = 1
    else:
        props['invalid_certificate'] = 0
    return props

def valid_certificate(content, props):
    if(props['unsolv_is_certificate'] == 'yes'):
        props['valid_certificate'] = 1
    else:
        props['valid_certificate'] = 0
    return props

def started_certificate_generation(content, props):
    props['begin_writing_certificate'] = int('Begin writing unsolvability certificate...' in content)
    return props

def generated_certificate(content, props):
    props['finished_certificate'] = int('Finished writing unsolvability certificate.' in content)
    return props



parser = Parser()
parser.add_pattern('unsolv_actions', 'Amount of Actions: (.+)', type=int, required=False)
parser.add_pattern('unsolv_total_time', 'Verify total time: (.+)', type=float, required=False)
parser.add_pattern('unsolv_is_certificate', 'Exiting: certificate is (.+)', type=str, required=False)
parser.add_pattern('unsolv_memory', 'Verify memory: (.+)KB', type=float, required=False)
parser.add_pattern('unsolv_abort_memory', 'abort memory (.+)KB', type=float, required=False)
parser.add_pattern('unsolv_abort_time', 'abort time (.+)s', type=float, required=False)
parser.add_pattern('unsolv_exit_message', 'Exiting: (.+)', type=str, required=False)
parser.add_pattern('certificate_size_kb', 'Certificate size: (.+)', type=int, required=False)
parser.add_pattern('verify_returncode', 'verify exit code: (.+)', type=int, required=False)
parser.add_pattern('unsolv_hints_size', 'File size of hints.txt: (.+)', type=int, required=False)
parser.add_pattern('unsolv_statebdd_size', 'File size of states.bdd: (.+)', type=int, required=False)
parser.add_pattern('unsolv_hcert_size', 'File size of h_cert.bdd: (.+)', type=int, required=False)
parser.add_pattern('certificate_generation_time', 'Certificate generation time: (.+)s', type=float, required=False)
parser.add_function(search_finished)
parser.add_function(is_cert)
parser.add_function(verify_finished)
parser.add_function(valid_certificate)
parser.add_function(invalid_certificate)
parser.add_function(verify_timeout)
parser.add_function(verify_oom)
parser.add_function(started_certificate_generation)
parser.add_function(generated_certificate)
parser.parse()
