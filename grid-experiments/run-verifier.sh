#!/bin/bash

#call verifier on certificate in $TMPDIR. use "time" to print its time usage if it doesn't print it by itself.
verify $TMPDIR/task.txt $TMPDIR/proof.txt #TODO voller Pfad
 
echo "verify exit code: $?"

if [ -n $SLURM_JOBID ]
then
    if [ "$(ls -A $TMPDIR)" ]
    then
        CERT_SIZE="Certificate size: "
        CERT_SIZE+=$(du -bck -- $TMPDIR/* | tail -n 1)
        echo $CERT_SIZE | sed -e "s/ total$//"
    else
        echo "Certificate size: 0"
    fi
    echo "File size of task.txt: `stat -c %s $TMPDIR/task.txt 2> nul`"
    echo "File size of proof.txt: `stat -c %s $TMPDIR/proof.txt 2> nul`"
    echo "File size of search.bdd: `stat -c %s $TMPDIR/search.bdd 2> nul`"
    # TODO: do we need the following lines?
    echo "File size of hints.txt: `stat -c %s $TMPDIR/hints.txt 2> nul`"
    echo "File size of states.bdd: `stat -c %s $TMPDIR/states.bdd 2> nul`"
    echo "File size of h_cert.bdd: `stat -c %s $TMPDIR/h_cert.bdd 2> nul`"
fi
